This project provides a test suite for the "unicode-graphemes" and
"unicode-graphemes-for-python" projects.

Most of the test data is taken from these articles:

  https://tonsky.me/blog/unicode/
  https://tonsky.me/blog/emoji/

The test suite is free software. See the file "LICENSE.txt" for license terms.
