#!/bin/sh

trap 'rm -fr $tmpfiles' 1 2 3 15
tmpfiles='emoji.out'

: ${UNICODE_GRAPHEMES=unicode-graphemes}
${UNICODE_GRAPHEMES} --show-glyphs emoji.in > emoji.out

: ${DIFF=diff}
${DIFF} emoji-show-glyphs.expected emoji.out
result=$?

rm -fr $tmpfiles

exit $result
