#!/bin/sh

trap 'rm -fr $tmpfiles' 1 2 3 15
tmpfiles='unicode.out'

: ${UNICODE_GRAPHEMES=unicode-graphemes}
${UNICODE_GRAPHEMES} unicode.in > unicode.out

: ${DIFF=diff}
${DIFF} unicode.expected unicode.out
result=$?

rm -fr $tmpfiles

exit $result
