#!/bin/sh

set -e

tests='unicode.sh emoji.sh unicode-show-glyphs.sh emoji-show-glyphs.sh'
cd tests
for i in $tests
do
  ./$i
done
